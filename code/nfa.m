%--------------------------------------------------%
% vim: ts=4 sw=4 et ft=mercury
%--------------------------------------------------%
% Copyright (C) Matthew Bardsley.
% This file is distributed under the terms specified in README.md.
%--------------------------------------------------%
%
% File: nfa.m.
% Main authors: mhbardsley.
% Stability: high.
%
% This module provides a finite state type and utility
% predicates that operate on it.
%
%--------------------------------------------------%
%--------------------------------------------------%

:- module nfa.
:- interface.

:- import_module set.
:- import_module int.
:- import_module regex.

%--------------------------------------------------%

:- type transition(T)
    --->    transition(T)
    ;       epsilon_transition.

:- type automaton(T)
    --->    automaton(
                states :: set(int), % Q
                finals :: set(int), % F
                transitions :: set(state_transition(T))   % delta
            ).

:- type state_transition(T)
    --->    state_transition(
                start_state :: int,
                transition :: transition(T),
                end_state :: int
            ).

%--------------------------------------------------%
%
% Automata creation.
%

% This section contains all the code that deals with generating an
% automaton.

	% from_regex(Expression, Automaton)
	% 
    % True iff `Automaton' is the result of converting regular
    % expression `Expression' to a non-deterministic finite
    % automaton, i.e. `Expression' and `Automaton' represent the same
    % language.
	%
:- pred from_regex(expression(T), automaton(T)).
:- mode from_regex(in, out) is det.

%--------------------------------------------------%
%--------------------------------------------------%

:- implementation.

:- import_module list.

%--------------------------------------------------%
%
% Automata creation.
%

    % add_to_states(AddVal, Automaton, NewAutomaton)
    %
    % True iff `NewAutomaton' is the result of adding `AddVal'
    % to int fields in `Automaton', i.e. state labels have been
    % shifted by `AddVal'.
    %
:- pred add_to_states(int, automaton(T), automaton(T)).
:- mode add_to_states(in, in, out) is det.

    % transition_plus(AddVal, Transition) = NewTransition
    % `NewTransition' is `Transition' with state values added to.
    %
:- func transition_plus(int, state_transition(T)) = 
    state_transition(T).

    % concat_transitions(ToState, FromState, NewTransition)
    % True iff `NewTransition' is an epsilon transition from
    % `FromState' to `ToState'.
    %
:- pred concat_transitions(int, int, state_transition(T)).
:- mode concat_transitions(in, in, out) is det.

    % kleene_transition(FinalState, Transition)
    % True iff `Transition' is an epsilon transition from `FinalState'
    % to 1.
    %
:- pred kleene_transition(int, state_transition(T)).
:- mode kleene_transition(in, out) is det.

from_regex(epsilon, automaton(Singleton, Singleton, init)) :-
    singleton_set(0, Singleton).

from_regex(lit(Literal), automaton(Q, F, Delta)) :-
    list_to_set([0, 1], Q),
    list_to_set([1], F),
    list_to_set([state_transition(0, transition(Literal), 1)], Delta).

from_regex(con(Ex1, Ex2), automaton(Q, F, Delta)) :-
    from_regex(Ex1, AutoA),
    from_regex(Ex2, AutoB),
    AutoA = automaton(QA, FA, DeltaA),
    count(QA, AddVal),
    add_to_states(AddVal, AutoB, automaton(QB, FB, DeltaB)),
    union(QA, QB, Q),
    F = FB,
    map(concat_transitions(AddVal), FA, NewD),
    Delta = union_list([DeltaA, DeltaB, NewD]).

from_regex(alt(Ex1, Ex2), automaton(Q, F, Delta)) :-
    from_regex(Ex1, AutoA),
    from_regex(Ex2, AutoB),
    AutoA = automaton(OldQA, _, _),
    count(OldQA, AddVal),
    add_to_states(1, AutoA, automaton(QA, FA, DeltaA)),
    add_to_states(1 + AddVal, AutoB, automaton(QB, FB, DeltaB)),
    singleton_set(0, Singleton),
    Q = union_list([QA, QB, Singleton]),
    union(FA, FB, F),
    list_to_set([state_transition(0, epsilon_transition, 1),
        state_transition(0, epsilon_transition, 1 + AddVal)], NewD),
    Delta = union_list([DeltaA, DeltaB, NewD]).

from_regex(kleene(Expression), automaton(Q, F, Delta)) :-
    from_regex(Expression, AutoEx),
    add_to_states(1, AutoEx, automaton(OldQ, OldF, OldDelta)),
    insert(0, OldQ, Q),
    insert(0, OldF, F),
    map(kleene_transition, F, NewDelta),
    union(OldDelta, NewDelta, Delta).

add_to_states(AddVal, automaton(InQ, InF, InD), automaton(Q, F, D)) :-
    Q = map(plus(AddVal), InQ),
    F = map(plus(AddVal), InF),
    D = map(transition_plus(AddVal), InD).

transition_plus(AddVal, state_transition(Start, Transition, End)) =
    state_transition(Start + AddVal, Transition, End + AddVal).

concat_transitions(ToState, FromState, 
    state_transition(FromState, epsilon_transition, ToState)).
    
kleene_transition(FinalState, 
    state_transition(FinalState, epsilon_transition, 1)).

%--------------------------------------------------%

:- end_module nfa.

%--------------------------------------------------%
%--------------------------------------------------%
