:- module test.
:- interface.
:- import_module io.
:- pred main(io::di, io::uo) is cc_multi.
:- implementation.
:- import_module regex, file_ops, list, set, language.

main(!IO) :-
	( try [io(!IO)] (
		read_direct_term("resources/expressions", Expressions,
	       		!IO)
	)
	then
		First = det_head(Expressions),
		limited_language_subset(First, 0, 12, 100, Language),
		fold(print_language, Language, !IO)
	catch S ->
		io.write_string(S, !IO),
		io.nl(!IO)
	).


:- pred print_language(list(symbol), io, io).
:- mode print_language(in, di, uo) is det.
print_language(List, !IO) :-
	io.print(List, !IO),
	io.nl(!IO).
