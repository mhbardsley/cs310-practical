%--------------------------------------------------%
% vim: ts=4 sw=4 et ft=mercury
%--------------------------------------------------%
% Copyright (C) Matthew Bardsley.
% This file is distributed under the terms specified in README.md.
%--------------------------------------------------%
%
% File: regex.m.
% Main authors: mhbardsley.
% Stability: high.
%
% This module provides a regular expression type and utility
% predicates that operate on it.
%
%--------------------------------------------------%
%--------------------------------------------------%

:- module regex.
:- interface.

:- import_module set.
:- import_module list.
:- import_module int.

%--------------------------------------------------%

:- type expression(T)
	--->	kleene(expression(T))   % *
    ;   kleene_plus(expression(T))  % +
	;	alt(expression(T), expression(T))   % +
    ;   alt_list(expression(T), expression(T), list(expression(T)))
	;	con(expression(T), expression(T))   % .
    ;   con_list(expression(T), expression(T), list(expression(T)))
	;	lit(T)  % literal
	;	epsilon.

%--------------------------------------------------%
%
% Language creation and tests.
%

% This section contains all the code that deals with generating a
% language, and checking a string belongs to the language.

	% language(Expression, Language)
	% 
	% True iff `Language' is the language given by `Expression',
	% i.e. if `Language' contains all words generated by
	% `Expression'.
	%
:- pred language(expression(T), set(list(T))).
:- mode language(in, out) is det.

    % limited_language(Expression, L, U, Language)
    %
    % True iff `Language' is the language given by `Expression', with
    % all elements of `Expression' having length no greater than `U'
    % and no lower than 'L', i.e. if `Language' contains all words 
    % generated by `Expression' and each word has no more than `U' 
    % and no fewer than `L' characters.
    %
:- pred limited_language(expression(T), int, int, set(list(T))).
:- mode limited_language(in, in, in, out) is det.

	% language_subset(Expression, N, LanguageSubset)
	%
	% True iff `LanguageSubset' contains only words in the
	% language given by `Expression', and has a cardinality
	% of no more than N.
	%
:- pred language_subset(expression(T), int, set(list(T))).
:- mode language_subset(in, in, out) is cc_multi.

    % limited_language_subset(Expression, MinLength, MaxLength, 
    %   MaxElements, LanguageSubset)
    %
    % True iff `LanguageSubset' contains only words in the language
    % given by `Expression' with cardinality no more than
    % `MaxElements', and with no element having length greater than
    % `MaxLength' or lower than `MinLength'.
    %
:- pred limited_language_subset(expression(T), int, int, int,
    set(list(T))).
:- mode limited_language_subset(in, in, in, in, out) is cc_multi.

	% language_member(Word, Expression)
	%
	% True iff `Word' belongs to the language given by
	% `Expression'.
	%
:- pred language_member(list(T), expression(T)).
:- mode language_member(in, in) is semidet.

%--------------------------------------------------%
%--------------------------------------------------%

:- implementation.

:- import_module solutions.
:- import_module bool.

%--------------------------------------------------%
%
% Language creation and tests.
%

% language_contains(Expression, N, Word)
    %
    % True iff `Word' belongs to the language given by `Expression',
    % and has length no greater than `U' and no lower than `L'.
    %
:- pred language_contains(int, int, expression(T), list(T)).
:- mode language_contains(in, in, in, out) is nondet.

    % check_limit(N, Item, More, SetIn, SetOut)
    %
    % True iff `SetOut' is SetIn with Item inserted if `More' is yes.
    % `More' is yes iff the cardinality of `SetIn' does not exceed
    % `N'.
    %
:- pred check_limit(int, list(T), bool, set(list(T)), set(list(T))).
:- mode check_limit(in, in, out, in, out) is det.

language(Expression, Language) :-
	solutions_set(language_contains(0, max_int, Expression), Language).

limited_language(Expression, L, U, Language) :-
    solutions_set(language_contains(L, U, Expression), Language).

language_subset(Expression, N, LanguageSubset) :-
	do_while(language_contains(0, max_int, Expression), 
        check_limit(N), init, LanguageSubset).

limited_language_subset(Expression, MinLength, MaxLength, MaxElements,
    LanguageSubset) :-
    do_while(language_contains(MinLength, MaxLength, Expression), 
        check_limit(MaxElements), init, LanguageSubset).

language_member(Word, Expression) :-
    language_contains(length(Word), length(Word), Expression, Word).

language_contains(L, U, kleene(Expression), Word) :-
    length(Word) =< U,
    length(Word) >= L,
	(
		Word = []
	;
	    language_contains(L, U, Expression, InsideStar),
		Word = InsideStar ++ Rest,
		language_contains(L, U - length(InsideStar), 
            kleene(Expression), Rest)
	).

language_contains(L, U, kleene_plus(Expression), Word) :-
    length(Word) =< U,
    length(Word) >= L,
	language_contains(L, U, Expression, InsideStar),
    language_contains(L, U - length(InsideStar), kleene(Expression), 
        Rest),
    Word = InsideStar ++ Rest.

language_contains(L, U, alt(Ex1, Ex2), Word) :-
    length(Word) =< U,
    length(Word) >= L,
	language_contains(L, U, Ex1, Word1),
	language_contains(L, U, Ex2, Word2),
	(Word = Word1 ; Word = Word2).

language_contains(L, U, alt_list(Ex1, Ex2, Exs), Word) :-
    ( if
        Exs = [Ex | Rest]
    then
        language_contains(L, U, alt(Ex1, alt_list(Ex2, Ex, Rest)),
            Word)
    else
        language_contains(L, U, alt(Ex1, Ex2), Word)
    ).

language_contains(L, U, con(Ex1, Ex2), Word) :-
    A + C = L,
    B + D = U,
    member(A, 0 .. L),
    member(C, 0 .. L),
    member(B, A .. U),
    member(D, C .. U),
	language_contains(A, B, Ex1, Word1),
	language_contains(C, D, Ex2, Word2),
	Word = Word1 ++ Word2.

language_contains(L, U, con_list(Ex1, Ex2, Exs), Word) :-
    ( if
        Exs = [Ex | Rest]
    then
        language_contains(L, U, con(Ex1, con_list(Ex2, Ex, Rest)),
            Word)
    else
        language_contains(L, U, con(Ex1, Ex2), Word)
    ).

language_contains(L, U, lit(Literal), [Literal]) :-
    1 =< U,
    1 >= L.

language_contains(L, U, epsilon, []) :-
    0 =< U,
    0 >= L.

check_limit(N, Word, More, LanguageIn, LanguageOut) :-
	count(LanguageIn, Length),
	( if
		N =< Length
	then
		More = no,
		LanguageIn = LanguageOut
	else
		More = yes,
		insert(Word, LanguageIn, LanguageOut)	
	).

%--------------------------------------------------%

:- end_module regex.

%--------------------------------------------------%
%--------------------------------------------------%
