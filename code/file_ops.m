%--------------------------------------------------%
% vim: ts=4 sw=4 et ft=mercury
%--------------------------------------------------%
% Copyright (C) Matthew Bardsley.
% This file is distributed under the terms specified in README.md.
%--------------------------------------------------%
%
% File: file_ops.m.
% Main authors: mhbardsley.
% Stability: high.
%
% This module provides more useful file operations that will throw
% an exception rather than return a contained result.
%
%--------------------------------------------------%
%--------------------------------------------------%

:- module file_ops.
:- interface.

:- import_module io.

%--------------------------------------------------%
%
% Reading terms from a file.
%

% This section deals with reading a term, or terms, directly from
% a file.

    % read_direct_term(FilePath, Term, !IO)
    %
    % Throws an exception if error in reading term or opening file.
    % True iff `Term' is the result of reading a term from the file
    % located at `FilePath', opening and closing an input stream to 
    % do so.
    %
:- pred read_direct_term(string, T, io, io).
:- mode read_direct_term(in, out, di, uo) is det.

%--------------------------------------------------%
%
% Opening streams.
%

% This seciton deals with opening streams directly, throwing errors
% if encountered.

    % open_output_write(FilePath, Stream, !IO)
    %
    % Throws an exception if cannot open file.
    % True iff `Stream' is the result of opening `FilePath' to write.
    %
:- pred open_output_write(string, io.text_output_stream, io, io).
:- mode open_output_write(in, out, di, uo) is det.

%--------------------------------------------------%
%--------------------------------------------------%

:- implementation.

:- import_module string.
:- import_module exception.

%--------------------------------------------------%
%
% Reading terms from a file.
%

    % make_error_message(TypeOfAcces, FilePath, Error)
    % 
    % True iff message is the result of constructing an error
    % message from `TypeOfAccess', `FilePath' and `Error', and it is
    % thrown to the calling predicate.
    %
:- pred make_error_message(string, string, string).
:- mode make_error_message(in, in, in) is erroneous.

read_direct_term(FilePath, Term, !IO) :-
	io.open_input(FilePath, StreamRes, !IO),
	(
		StreamRes = ok(Stream),
		io.read(Stream, Res, !IO),
		io.close_input(Stream, !IO),
		(
			Res = ok(Term)
		;
			Res = eof,
			make_error_message("read", FilePath, "no data present")
		;
			Res = error(Message, Line),
			make_error_message("read", FilePath, Message ++ 
                " on line "  ++ LineStr),
			int_to_string(Line, LineStr)
		)
	;
		StreamRes = error(ErrorCode),
		io.error_message(ErrorCode, Error),
		make_error_message("read", FilePath, Error)
	).

make_error_message(TypeOfAccess, FilePath, Error) :-
	Message = "When trying to " ++ TypeOfAccess ++ " " ++ FilePath ++ 
		", " ++ Error,
	throw(Message).

%--------------------------------------------------%
%
% Opening streams.
%

open_output_write(FilePath, Stream, !IO) :-
    io.open_output(FilePath, StreamRes, !IO),
    (
        StreamRes = ok(Stream)
    ;
        StreamRes = error(ErrorCode),
        io.error_message(ErrorCode, Error),
        make_error_message("write", FilePath, Error)
    ).

%--------------------------------------------------%

:- end_module file_ops.

%--------------------------------------------------%
%--------------------------------------------------%
