%--------------------------------------------------%
% vim: ts=4 sw=4 et ft=mercury
%--------------------------------------------------%
% Copyright (C) Matthew Bardsley.
% This file is distributed under the terms specified in README.md.
%--------------------------------------------------%
%
% File: music_generator.m.
% Main authors: mhbardsley.
% Stability: high.
%
% This module generates music, by taking a regular expression,
% generating accepting strings and then converting them to a MIDI
% output.
%
%--------------------------------------------------%
%--------------------------------------------------%

:- module music_generator.
:- interface.

:- import_module io.

%--------------------------------------------------%
%
% Music generation.
%

% This section contains all of the code that deals with generating
% music.

	% generate(InFilePath, OutFilePath, !IO)
	%
	% True iff regular expression is taken from `InFilePath',
	% and music is written to via `OutFilePath' using I/O.
	%

:- pred main(io, io).
:- mode main(di, uo) is cc_multi.

%--------------------------------------------------%
%--------------------------------------------------%

:- implementation.

:- import_module file_ops.
:- import_module regex.
:- import_module language.
:- import_module list.
:- import_module set.
:- import_module calendar.
:- import_module string.
:- import_module int.

%--------------------------------------------------%

main(!IO) :-
    ( try [io(!IO)] (
        read_direct_term("resources/expression", Expression, !IO),
        limited_language_subset(Expression, 0, 12, 100, Pieces),
        to_sorted_list(Pieces, PieceList),
        length(PieceList, PieceListLength),
        Indices = 0 .. PieceListLength - 1,
        foldl(to_midi(PieceList), Indices, !IO)
    )
    then
        io.write_string("Successfully written pieces\n", !IO)
    catch S ->
        io.write_string(S, !IO),
        io.nl(!IO)
    ).

:- pred to_midi(list(list(symbol)), int, io, io).
:- mode to_midi(in, in, di, uo) is det.

to_midi(PieceList, Index, !IO) :-
    det_index0(PieceList, Index, Piece),
    io.print(Piece, !IO),
    io.nl(!IO),
    to_musicxml(Piece, MusicXML),
    int_to_string(Index, IndexStr),
    FilePath = "generated/" ++ IndexStr,
    open_output_write(FilePath ++ ".xml", Stream, 
        !IO),
    io.write_string(Stream, MusicXML, !IO),
    io.close_output(Stream, !IO),
    io.call_system("musicxml2mid/musicxml2mid """ ++
        FilePath ++ ".xml"" > """ ++ FilePath ++ ".mid""", 
        _, !IO).

%--------------------------------------------------%

:- end_module music_generator.

%--------------------------------------------------%
%--------------------------------------------------%
