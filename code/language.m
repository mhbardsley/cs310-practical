%--------------------------------------------------%
% vim: ts=4 sw=4 et ft=mercury
%--------------------------------------------------%
% Copyright (C) Matthew Bardsley.
% This file is distributed under the terms specified in README.md.
%--------------------------------------------------%
%
% File: language.m.
% Main authors: mhbardsley.
% Stability: high.
%
% This module provides a symbol type to restrict the values a
% string can take when processing regular expressions. It also
% defines the predicates that convert a sequence of symbols to
% MusicXML files.
%
%--------------------------------------------------%
%--------------------------------------------------%

:- module language.
:- interface.

:- import_module list.

%--------------------------------------------------%

:- type symbol
	--->	note_a
	;	note_b
	;	note_c
	;	note_d
	;	note_e
	;	note_f
	;	note_g
    ;   zero_octave
    ;   one_octave
    ;   two_octave
    ;   three_octave
    ;   four_octave
    ;   five_octave
    ;   six_octave
    ;   seven_octave
    ;   eight_octave
	;	zero
	;	one
	;	two
	;	three
	;	four
	;	five
	;	six
	;	seven
	;	eight
	;	nine
    ;   slash.

:- type fraction
    --->    fraction(
                numerator :: int,    % numerator of the fraction
                denominator :: int
            ).

%--------------------------------------------------%
%
% MusicXML generation.
%

% This section contains all of the code that deals with generating
% MusicXML files.

    % to_musicxml(Sequence, MusicXML)
    %
    % Throws an exception if sequence of music is not valid.
    % True iff `MusicXML' is the result of converting the sequence
    % of language symbols, `Sequence' (of a conformant type) to
    % a MusicXML type.
    %
:- pred to_musicxml(list(symbol), string).
:- mode to_musicxml(in, out) is det.

%--------------------------------------------------%
%--------------------------------------------------%

:- implementation.

:- import_module string.
:- import_module int.
:- import_module require.

%--------------------------------------------------%
%
% MusicXML generation
%

    % get_time_signature(Sequence, Numerator, Denominator)
    %
    % True iff `Numerator' and `Denominator' represent the top
    % and bottom fractional elements of adding note lengths in
    % `Sequence' together
    %
:- pred get_time_signature(list(symbol), int, int).
:- mode get_time_signature(in, out, out) is det.

    % format_time_signature(Numerator, TimeSig)
    %
    % True iff `TimeSig' is the result of formatting a time signature
    % with numerator `Numerator' and denominator 4 in MusicXML tag 
    % format.
    %
:- pred format_time_signature(int, string).
:- mode format_time_signature(in, out) is det.

    % format_divisions(Denominator, Divisions)
    %
    % True iff `Divisions' is the result of formatting `Denominator'
    % into a MusicXML divisions tag.
    %
:- pred format_divisions(int, string).
:- mode format_divisions(in, out) is det.

    % get_notes(Sequence, Denominator, Notes)
    %
    % True iff `Notes' is the result of getting a chronological
    % list of notes in MusicXML tag format, with assigned lengths 
    % multiplied by `Denominator' each time.
    %
:- pred get_notes(list(symbol), int, string).
:- mode get_notes(in, in, out) is det.

    % format_musicxml(Divisions, TimeSig, Notes, MusicXML)
    %
    % True iff `MusicXML' is a string representing a MusicXML file
    % with divisions `Divisions', time signature `TimeSig', and 
    % notes `Notes'.
    %
:- pred format_musicxml(string, string, string, string).
:- mode format_musicxml(in, in, in, out) is det.

to_musicxml(Sequence, MusicXML) :-
    get_time_signature(Sequence, Numerator, Denominator),
    format_time_signature(Numerator, TimeSig),
    format_divisions(Denominator, Divisions),
    get_notes(Sequence, Denominator, Notes),
    format_musicxml(Divisions, TimeSig, Notes, MusicXML).

    % get_fractions(Sequence, Fractions)
    %
    % True iff `Fractions' is the result of extracting a sequence
    % of fractions from `Sequence'.
    %
:- pred get_fractions(list(symbol), list(fraction)).
:- mode get_fractions(in, out) is det.

    % add_fractions(Fractions, Numerator, Denominator)
    %
    % True iff `Numerator' and `Denominator' are the respective
    % numerator and denominator values when adding the list of
    % fractions, `Fractions'.
    %
:- pred add_fractions(list(fraction), int, int).
:- mode add_fractions(in, out, out) is det.

    % get_note(SequenceIn, SequenceOut, Denominator, Note)
    %
    % True iff `Note' is the result of converting the first note of
    % `SequenceIn' to MusicXML format, such that its duration is the
    % given length multiplied by the `Denominator', and `SequenceOut'
    % is the list of symbols representing the remaining notes.
    %
:- pred get_note(list(symbol), list(symbol), int, string).
:- mode get_note(in, out, in, out) is det.

get_time_signature(Sequence, Numerator, Denominator) :-
    get_fractions(Sequence, Fractions),
    add_fractions(Fractions, Numerator, Denominator).

format_time_signature(Numerator, TimeSig) :-
    int_to_string(Numerator, NumString),
    TimeSig = "<time><beats>" ++ NumString ++ 
        "</beats><beat-type>4</beat-type></time>".

format_divisions(Denominator, Divisions) :-
    int_to_string(Denominator, DenString),
    Divisions = "<divisions>" ++ DenString ++ "</divisions>".

get_notes(Sequence, Denominator, Notes) :-
    ( if
        Sequence = []
    then
        Notes = ""
    else
        get_note(Sequence, SequenceOut, Denominator, NoteStr),
        get_notes(SequenceOut, Denominator, OtherNotes),
        Notes = NoteStr ++ OtherNotes
    ).

format_musicxml(Divisions, TimeSig, Notes, MusicXML) :-
   "<?xml version=""1.0"" encoding=""UTF-8"" standalone=""no""?>\n\
   <!DOCTYPE score-partwise PUBLIC\n\
   \t""-//Recordare//DTD MusicXML 3.1 Partwise//EN""\n\
   \t""http://www.musicxml.org/dtds/partwise.dtd"">\n\
   <score-partwise version=""3.1"">\n\
   \t<part-list>\n\
   \t\t<score-part id=""P1"">\n\
   \t\t\t<part-name>Music</part-name>\n\
   \t\t</score-part>\n\
   \t</part-list>\n\
   \t<part id=""P1"">\n\
   \t\t<measure number=""1"">\n\
   \t\t\t<attributes>"
   ++ "\n" ++ Divisions ++ "\n" ++
   "\t\t\t\t<key>\n\
   \t\t\t\t\t<fifths>0</fifths>\n\
   \t\t\t\t</key>"
   ++ "\n" ++ TimeSig ++ "\n" ++
   "\t\t\t\t<clef>\n\
   \t\t\t\t\t<sign>G</sign>\n\
   \t\t\t\t\t<line>2</line>\n\
   \t\t\t\t</clef>\n\
   \t\t\t</attributes>"
   ++ "\n" ++ Notes ++ "\n" ++
   "\t\t</measure>\n\
   \t</part>\n\
   </score-partwise>" = MusicXML.

    % not_part_of_fraction(Symbol)
    %
    % True iff `Symbol' is not one of the fractional symbols: i.e.,
    % not a number digit or solidus (slash).
    %
:- pred not_part_of_fraction(symbol).
:- mode not_part_of_fraction(in) is semidet.

    % part_of_fraction(Symbol)
    %
    % True iff `Symbol' is one of the fractional symbols: i.e., a
    % number digit or solidus (slash).
    %
:- pred part_of_fraction(symbol).
:- mode part_of_fraction(in) is semidet.

    % det_sequence_to_fraction(FractionSequence, Fraction)
    %
    % True iff `Fraction' is the fraction type representation of
    % `FractionSequence', i.e. digit values have been converted
    % to integer values, separated into numerator and denominator
    % values by the solidus (slash). Throws an error if the sequence
    % contains values that do not constitute a fraction.
    %
:- pred det_sequence_to_fraction(list(symbol), fraction).
:- mode det_sequence_to_fraction(in, out) is det.

    % add_fraction(Fraction1, Fraction2, Result)
    %
    % True iff `Result' is the result of adding `Fraction1' and
    % `Fraction2'.
    %
:- pred add_fraction(fraction, fraction, fraction).
:- mode add_fraction(in, in, out) is det.

    % note_to_string(Note, String)
    %
    % Throws an exception if `Note' is not a note symbol.
    % True iff `String' is the string representation of `Note'.
    %
:- pred note_to_string(symbol, string).
:- mode note_to_string(in, out) is det.

    % octave_to_string(Octave, String)
    %
    % Throws an exception if `Octave' is not an octave symbol.
    % True iff `String' is the string representation of `Octave'.
    %
:- pred octave_to_string(symbol, string).
:- mode octave_to_string(in, out) is det.

get_fractions(Sequence, Fractions) :-
    ( if
        Sequence = []
    then
        Fractions = []
    else
        drop_while(not_part_of_fraction, Sequence, StartOfFraction),
        take_while(part_of_fraction, StartOfFraction, FractionSeq,
            Remaining),
        det_sequence_to_fraction(FractionSeq, Fraction),
        get_fractions(Remaining, OtherFractions),
        Fractions = [Fraction | OtherFractions]
    ). 

add_fractions(Fractions, Numerator, Denominator) :- 
    foldl(add_fraction, Fractions, fraction(0, 1), 
        fraction(Numerator, Denominator)).

get_note(SequenceIn, SequenceOut, Denominator, NoteStr) :-
    ( if
        SequenceIn = [Note, Octave | FractionAndRest]
    then
        take_while(part_of_fraction, FractionAndRest, Fraction,
            SequenceOut),
        det_sequence_to_fraction(Fraction, fraction(Num, Den)),
        note_to_string(Note, NoteString),
        octave_to_string(Octave, OctaveStr),
        Value = (Denominator/Den) * Num,
        int_to_string(Value, LengthStr),
        NoteStr = "<note><pitch><step>" ++ NoteString ++ 
            "</step><octave>" ++ OctaveStr ++ "</octave>" ++ 
            "</pitch><duration>" ++ LengthStr ++ "</duration></note>"
    else
        unexpected($pred, "Did not encounter a note/octave")
    ).

not_part_of_fraction(Symbol) :-
    not part_of_fraction(Symbol).

part_of_fraction(Symbol) :-
    member(Symbol, [one, two, three, four, five, six, seven, eight,
            nine, slash]).

    % get_fraction_partition(FractionSequence, NumSequence, 
    %   DenSequence)
    %
    % True iff `NumSequence' and `DenSequence' is the result of
    % partitionining `FractionSequence' by slash, assuming there
    % is one.
    %
:- pred get_fraction_partition(list(symbol), list(symbol), 
    list(symbol)).
:- mode get_fraction_partition(in, out, out) is multi.

    % sequence_to_number(Sequence, Number)
    %
    % True iff `Number' is the result of converting the integer
    % formatted as a sequence of digits.
    %
:- pred sequence_to_number(list(symbol), int).
:- mode sequence_to_number(in, out) is det.

    % lcm(Den1, Den2, Lcm)
    %
    % True iff `Lcm' is the lowest positive integer which `Den1'
    % and `Den2' both divide.
    %
:- pred lcm(int, int, int).
:- mode lcm(in, in, out) is nondet.
:- mode lcm(in, in, in) is semidet.

det_sequence_to_fraction(FractionSequence, Fraction) :-
    ( if
        member(slash, FractionSequence)
    then
        ( if
            member_index0(slash, FractionSequence, Index1),
            member_index0(slash, FractionSequence, Index2)
            => Index1 = Index2
        then
            promise_equivalent_solutions [NumSequence, DenSequence]
                (get_fraction_partition(FractionSequence, NumSequence,
                DenSequence)),
            sequence_to_number(NumSequence, Numerator),
            sequence_to_number(DenSequence, Denominator),
            Fraction = fraction(Numerator, Denominator)
        else
            unexpected($pred, "More than one slash present in fraction")
        )
    else
        unexpected($pred, "Only one slash present in fraction")
    ).

add_fraction(fraction(Num1, Den1), fraction(Num2, Den2), 
    fraction(NewNum, NewDen)) :-
    ( if
        Den1 = 0 ; Den2 = 0
    then
        unexpected($pred, "Found fraction with zeroed denominator")
    else
        NewNum = (NewDen / Den1) * Num1 + (NewDen / Den2) * Num2,
        ( if
            promise_equivalent_solutions [Lcm] (
                lcm(Den1, Den2, Lcm)
            )
        then
            NewDen = Lcm
        else
            unexpected($pred, "Could not find Lcm")
        )
    ).

note_to_string(Note, String) :-
    ( if
        Note = note_a
    then
        String = "A"
    else if
        Note = note_b
    then
        String = "B"
    else if
        Note = note_c
    then
        String = "C"
    else if
        Note = note_d
    then
        String = "D"
    else if
        Note = note_e
    then
        String = "E"
    else if
        Note = note_f
    then
        String = "F"
    else if
        Note = note_g
    then
        String = "G"
    else
        unexpected($pred, "Symbol is not a note")
    ).

octave_to_string(Octave, String) :-
    ( if
        Octave = zero_octave
    then
        String = "0"
    else if
        Octave = one_octave
    then
        String = "1"
    else if
        Octave = two_octave
    then
        String = "2"
    else if
        Octave = three_octave
    then
        String = "3"
    else if
        Octave = four_octave
    then
        String = "4"
    else if
        Octave = five_octave
    then
        String = "5"
    else if
        Octave = six_octave
    then
        String = "6"
    else if
        Octave = seven_octave
    then
        String = "7"
    else if
        Octave = eight_octave
    then
        String = "8"
    else
        unexpected($pred, "Symbol is not an octave")
    ).

    % digit_value(Symbol, PowerIn, PowerOut, NumIn, NumOut)
    %
    % Throws exception if Symbol is not a digit.
    % True iff `NumOut' is the result of adding `Symbol' * `PowerIn'
    % to `NumIn', i.e. adding a digit `Symbol' in a certain position 
    % `PowerIn'.
    %
:- pred digit_value(symbol, int, int, int, int).
:- mode digit_value(in, in, out, in, out) is det.

get_fraction_partition(FractionSequence, NumSequence, DenSequence) :-
    ( if
        member_index0(slash, FractionSequence, Index)
    then
        det_split_list(Index, FractionSequence, NumSequence, DenSeqPlus),
        DenSequence = det_tail(DenSeqPlus)
    else
        unexpected($pred, "Could not find a slash in fraction")
    ).

sequence_to_number(Sequence, Number) :-
    foldr2(digit_value, Sequence, 0, _, 0, Number).

lcm(Den1, Den2, Lcm) :-
    member(Lcm, 1 .. Den1 * Den2),
    Lcm mod Den1 = 0,
    Lcm mod Den2 = 0,
    all [X] (
        member(X, 1 .. Lcm - 1) => not lcm(Den1, Den2, X)
    ).

digit_value(Symbol, PowerIn, PowerOut, NumIn, NumOut) :-
    PowerOut = PowerIn + 1,
    pow(10, PowerIn, Mult),
    ( if
        Symbol = one
    then
        NumOut = NumIn + (1 * Mult)
    else if
        Symbol = two
    then
        NumOut = NumIn + (2 * Mult)
    else if
        Symbol = three
    then
        NumOut = NumIn + (3 * Mult)
    else if
        Symbol = four
    then
        NumOut = NumIn + (4 * Mult)
    else if
        Symbol = five
    then
        NumOut = NumIn + (5 * Mult)
    else if
        Symbol = six
    then
        NumOut = NumIn + (6 * Mult)
    else if
        Symbol = seven
    then
        NumOut = NumIn + (7 * Mult)
    else if
        Symbol = eight
    then
        NumOut = NumIn + (8 * Mult)
    else if
        Symbol = nine
    then
        NumOut = NumIn + (9 * Mult)
    else
        unexpected($pred, "Symbol is not a number")
    ).

%--------------------------------------------------%

:- end_module language.

%--------------------------------------------------%
%--------------------------------------------------%
